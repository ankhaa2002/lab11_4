import React from 'react';
import {AppRegistry, Image, PixelRatio, StyleSheet, Text, TouchableOpacity, View,} from 'reactnative';
import ImagePicker from 'react-native-image-picker';
export default class App extends React.Component {
     state = {
         avatarSource: null,
         videoSource: null,
     };
     constructor(props) {
         super(props);
         this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
         this.selectVideoTapped = this.selectVideoTapped.bind(this);
     }
     selectPhotoTapped() {
          const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
          skipBackup: true,
          },
     };
      ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = { uri: response.uri };
          this.setState({
            avatarSource: source,
          });
          }
      });
     }
      selectVideoTapped() {
           const options = {
           title: 'Video Picker',
           takePhotoButtonTitle: 'Take Video...',
           mediaType: 'video',
           videoQuality: 'medium',
       };
       ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
           if (response.didCancel) {
            console.log('User cancelled video picker');
           } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
           } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
           } else {
               this.setState({
               videoSource: response.uri,
               });
           }
           });
      }
       render() {
        return (
        <View style={styles.container}>

        <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>

        <View
            style={[
            styles.avatar,
            styles.avatarContainer,
            { marginBottom: 20 },
            ]}
        >
        {this.state.avatarSource === null ? (
            <Text>Select a Photo</Text>
        ) : (
            <Image style={styles.avatar} source={this.state.avatarSource} />
        )}
        </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.selectVideoTapped.bind(this)}>
            <View style={[styles.avatar, styles.avatarContainer]}>
                <Text>Select a Video</Text>
            </View>
        </TouchableOpacity>
        {this.state.videoSource && (
        <Text style={{ margin: 8, textAlign: 'center' }}>
        {this.state.videoSource}
        </Text>
        )}
        </View>
        );
    }
}